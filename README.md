# README #

* pyLivereload is a small script that allows live reloading of a web page (Chrome WebDriver) when there are file changes on a monitored folder
* Looks for changes in css, js or html files in the folder
* Creates an instance of the web driver and serves the folder through an HTTP server on the localhost

Why?

* I wanted to do some experimenting with Phaser framework and be able to update in real time my code, but I had no luck at all making standard LiveReload to work in my Windows computer. So I did this small script and worked great.

Features

* Live reloading of the page when changes in files are detected
* Command line receives log output from the browser (only SEVERE exceptions)

What is needed?

* Chrome web driver (path needs to be specified in the code) https://sites.google.com/a/chromium.org/chromedriver/downloads
* Python Libraries: selenium, os, threading, urllib, posixpath, SimpleHTTPServer, BaseHTTPServer, time

How to Use ?

* Specify in the code the folder to be monitored, the server port and the types of extensions to be monitored
* run the code from the command line : python pyLiveReload.py or from your desired IDE