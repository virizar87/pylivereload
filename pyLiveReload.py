from selenium import webdriver
import os
import threading
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import urllib
import posixpath
from SimpleHTTPServer import SimpleHTTPRequestHandler
from BaseHTTPServer import HTTPServer
import time
from selenium.common.exceptions import WebDriverException


class RootedHTTPServer(HTTPServer):
    """ Rooted Http server, receives a directory as an input paramaeters
    where the server will be started
    """

    def __init__(self, base_path, *args, **kwargs):
        HTTPServer.__init__(self, *args, **kwargs)
        self.RequestHandlerClass.base_path = base_path


class RootedHTTPRequestHandler(SimpleHTTPRequestHandler):
    """ Rooted Http request handler, overrides method translate_path to get
    a known root instead of the os.curdir default
    """

    def translate_path(self, path):
        """Translate a /-separated PATH to the local filename syntax.

        Components that mean special things to the local file system
        (e.g. drive or directory names) are ignored.  (XXX They should
        probably be diagnosed.)
        """

        path = posixpath.normpath(urllib.unquote(path))
        words = path.split('/')
        words = filter(None, words)
        path = self.base_path
        for word in words:
            drive, word = os.path.splitdrive(word)
            head, word = os.path.split(word)
            if word in (os.curdir, os.pardir):
                continue
            path = os.path.join(path, word)
        return path


class ThreadedClient:
    """Threaded client, handles the Http server and the webdriver
    updating with folder monitoring
    """

    def __init__(self, workDir):

        #TODO: Add firefox driver (include as an input to the class constructor)
        # Default Port for Server
        self.port = 8000
        # Working Directory to monitor
        self.workDir = workDir
        # Filetypes to monitor in the folders
        self.fileTypes = ['html', 'js', 'css']
        # Server Addres (localhost)
        self.serverAddress = ('', self.port)
        # Insstance of the http server on the monitored folder
        self.httpd = RootedHTTPServer(self.workDir, self.serverAddress, RootedHTTPRequestHandler)
        # Web driver setup for chrome
        d = DesiredCapabilities.CHROME
        d['loggingPrefs'] = {'browser': 'ALL'}      # Outputs all messages of the browser console to the shell
        # Chrome driver location
        self.driver = webdriver.Chrome('C:/chromeDriver/chromedriver.exe', desired_capabilities=d)

    def run(self):
        """ Run the server, first launch a thread with the driver
        then run the server in the main thread"""

        print 'Starting PyLiveReload \n'
        print '[+] Monitoring the following folder: %s' % self.workDir

        threading.Thread(target=self.web_driver).start()

        self.server_thread()

    def web_driver(self):
        """ Web drivere method starts the web driver, builds the
        file list to be monitored in the desired folder, and
        monitors changes in the files in the folder
        """

        # Start the webdriver
        self.driver.get('localhost:%i' % self.port)

        # Build the file list to be monitored
        fileList = self.build_file_list()

        # Get the initial modification times of the files in the folder
        initTimes = [os.path.getmtime(file) for file in fileList]

        # While the webdriver is running
        while self.driver.current_url and webdriver:

            try:

                # Check the modification times of the files
                newTimes = [os.path.getmtime(file) for file in fileList]

                # Loop through the file list and check if the files have been modified,
                # If so, reload the window. Print the error log of the driver console to
                # the shell
                for i, t in enumerate(newTimes):
                    if t != initTimes[i]:
                        initTimes[i] = t
                        self.driver.get(str(self.driver.current_url))
                        for msg in self.driver.get_log('browser'):
                            if msg['level'] == 'SEVERE':
                                print '[%s] %s' % (msg['level'].upper(), msg['message'])

                # Refresh time of the loop, can be modified, should be at least 0.1 to avoid high cpu usage
                time.sleep(0.1)

            # If the window is closed, break the execution
            except WebDriverException:
                break

        # Shutdown the webdriver and the server
        print '[+] Shutting Down Web Driver'
        self.httpd.shutdown()

    def build_file_list(self):
        """ Build the fill list of the desired folder by
        recursively inspecting and selecting the contents
        of the desired selected folder
        """

        dirTree = os.walk(self.workDir)

        monitoredFiles = []

        for dirList in dirTree:
            for col in dirList[2]:
                for fileType in self.fileTypes:
                    if fileType in col.split('.')[-1]:
                        monitoredFiles.append(os.path.join(dirList[0], col))

        return monitoredFiles

    def server_thread(self):
        """ Server thread runs http server until stopped
        by user or by exception
        """

        sa = self.httpd.socket.getsockname()
        print "[+] Serving HTTP on", sa[0], "port", sa[1], "..."

        self.httpd.serve_forever()
        print '[+] Shutting Down Server'

if __name__ == '__main__':

    # Path to the folder to be monitored
    monitoringFolder = 'C:/Users/Victor/Desktop/Phaser/Tutorial2'

    # Run the live reloader!
    ThreadedClient(monitoringFolder).run()